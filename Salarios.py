class Empleado:
    def _init_(self, nombre, nomina):
        self.nombre = nombre
        self.nomina = nomina
    
    def impuesto_por_pagar(self):
        return self.nomina * 0.30
        
    def _str_(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.calculo_impuestos())