from tkinter import E


class Empleado:
    def __init__ (self, nombre, salario):
        self.nombre = nombre
        self.salario = salario
    
    def impuesto_por_pagar(self):
        return self.salario * 0.30
        
    def __str__(self):
        return "El empleado {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.impuesto_por_pagar())

class Jefe(Empleado):

    def __init__(self, nombre, salario, bonus=0):
        super().__init__(nombre, salario)
        self.bonus=bonus

    def impuesto_total(self):
        return super().impuesto_por_pagar() + self.bonus*0.30
    
    def __str__(self):
        return "El jefe {name} debe pagar {tax:.2f}".format(name=self.nombre, tax=self.impuesto_total()) 


e1=(Empleado("Pepe", 20000))
j1= Jefe("Ana", 30000, 2000)

print(e1)#llama al método del str del empleado
print(j1)#llama al str del empleado si no se pone un str nuevo en la class Jefe
